$(function() {
  // Handle to-top button
  $('[action=totop]').each(function() {
    $(this).click(function() {
      $('html, body').stop().animate({
        scrollTop: 0
      }, 1000);
    });
  });

  // Handle header intro
  setTimeout(function() {
    $('body > header').attr('loaded', true).fadeTo(500, 1).css('overflow', 'visible');
  }, 250);
});