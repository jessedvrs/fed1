var pageroot = 'pages/';
var pageElementSelector = 'main';

// Specify generic page construction
var page = function(title, file) {
  this.title = title;
  this.file = file;
};

// Create pages construction
var pages = {};
pages.info = new page('Informatie', 'info.html');
pages.registreren = new page('Aanmelden', 'registreren.html');
pages.registreren_project = new page('Project', 'registreren/project.html');
pages.registreren_stagebedrijf = new page('Stage', 'registreren/stagebedrijf.html');

// Default page (slug)
var currentPage = 'info';

// Define switchPage function
var switchPage = function(page) {
  var filename, first_slug;
  if(pages[page] !== undefined) {
    filename = pages[page].file;
    currentPage = page;
  } else {
    filename = '404.html';
  }
  first_slug = page.split('_');
  $(pageElementSelector).stop().fadeTo(200, 0, function() {
    $('html, body').scrollTop(0);
    $(pageElementSelector).load(pageroot + filename, function() {
      $(window).trigger('paged');
      if($('angular').length > 0) {
        angular.bootstrap(angular.element('angular')[0]);
      }
      $('nav > ul > li').attr('state', 'inactive');
      $('nav > ul > li[page = ' + first_slug[0] + ']').attr('state', 'active');
      $(pageElementSelector).fadeTo(200, 1);
      $('video').load();
    });
  });
};

// Initialize
$(function() {
  switchPage(currentPage);
  $(window).bind('hashchange', function() {
    var currentSlug = window.location.hash.substr(1);
    switchPage(currentSlug);
  });
});