$(window).on('paged', function() {
  if($('html').hasClass('touch')) {
    $elms = $('[hovertouch]').bind('touchend', function(event) {
      $elm = $(event.currentTarget);
      $elm.toggleClass('hover');
    });
  }
});