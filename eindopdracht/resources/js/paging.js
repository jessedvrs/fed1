var pageroot = 'pages/';
var pageElementSelector = 'main';
var defaultPage = 'home';

// Specify generic page construction
var page = function(title, file) {
  this.title = title;
  this.file = file;
};

// Create pages construction
var pages = {};
pages.home = new page('Home', 'home.html');
pages.opleiding = new page('Opleiding', 'opleiding.html');
pages.opleiding_visieenbeleid = new page('Visie en beleid', 'opleiding/visieenbeleid.html');
pages.opleiding_opbouwstudieprogramma = new page('Opbouw studieprogramma', 'opleiding/opbouwstudieprogramma.html');
pages.opleiding_competenties = new page('Competenties', 'opleiding/competenties.html');
pages.opleiding_diploma = new page('Diploma', 'opleiding/diploma.html');
pages.opleiding_beroepen = new page('Beroepen', 'opleiding/beroepen.html');
pages.onderwijsprogramma = new page('Onderwijsprogramma', 'onderwijsprogramma.html');
pages.organisatie = new page('Organisatie', 'organisatie.html');
pages.registreren = new page('Aanmelden', 'registreren.html');
pages.registreren_project = new page('Project', 'registreren/project.html');
pages.registreren_stagebedrijf = new page('Stage', 'registreren/stagebedrijf.html');
pages.error404 = new page('Error', '404.html');

var currentPage = defaultPage;

// Define switchPage function
var switchPage = function(page) {
  var filename, first_slug;
  if(page === '') {
    page = defaultPage;
  }
  if(pages[page] !== undefined) {
    filename = pages[page].file;
    currentPage = page;
  } else {
    filename = pages['error404'].file;
    currentPage = 'error404';
  }
  first_slug = page.split('_');
  $(pageElementSelector).stop().fadeTo(200, 0, function() {
    $('html, body').scrollTop(0);
    $(pageElementSelector).load(pageroot + filename, function() {
      $(window).trigger('paged');
      if($('angular').length > 0) {
        angular.bootstrap(angular.element('angular')[0]);
      }
      $('nav > ul > li').attr('state', 'inactive');
      $('nav > ul > li[page = ' + first_slug[0] + ']').attr('state', 'active');
      $(pageElementSelector).fadeTo(200, 1);
      $('video').load();
    });
  });
};

// Initialize
$(function() {
  switchPage(currentPage);
  $(window).bind('hashchange', function() {
    var currentSlug = window.location.hash.substr(1);
    switchPage(currentSlug);
  });
});